﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Tokenattributes;
using System.Text;

namespace WebSamples
{
    public class LuceneCamelCaseFilter: TokenFilter
    {
         private readonly TermAttribute _termAttr;
        private readonly PositionIncrementAttribute _posIncAttr;

        private State _current;
        private Queue<string> _pascalWords = new Queue<string>();

        public LuceneCamelCaseFilter(TokenStream stream)
            : base(stream)
        {
            _termAttr = (TermAttribute) this.AddAttribute(typeof(TermAttribute));
            _posIncAttr = (PositionIncrementAttribute)this.AddAttribute(typeof(PositionIncrementAttribute));
        }

        public override bool IncrementToken()
        {
            if (_pascalWords.Count > 0)
            {
                RestoreState(_current);

                _termAttr.SetTermBuffer(_pascalWords.Dequeue());
                _posIncAttr.SetPositionIncrement(0);
                return true;
            }

            if (!input.IncrementToken())
            {
                return false;
            }

            PascalCaseSplit();

            if (_pascalWords.Count > 0)
            {
                _current = CaptureState();
            }
            return true;
        }

        private void PascalCaseSplit()
        {
            var builder = new StringBuilder();
            var buffer = _termAttr.TermBuffer();

            for (int i = 0; i < _termAttr.TermLength(); i++)
            {
                var c = buffer[i];

                if (char.IsUpper(c) && builder.Length > 0)
                {
                    _pascalWords.Enqueue(builder.ToString());
                    builder.Clear();
                }
                builder.Append(c);
            }

            if (_pascalWords.Count > 0 && builder.Length > 0)
            {
                _pascalWords.Enqueue(builder.ToString());
            }
        }
    }
}