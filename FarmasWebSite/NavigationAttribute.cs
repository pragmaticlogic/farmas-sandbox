﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using WebSamples.Models.ViewModels;

namespace WebSamples
{
    [AttributeUsage(AttributeTargets.Method)]
    public class NavigationAttribute: Attribute
    {
        public string Section { get; set; }
        public string Tab { get; set; }

        public NavigationAttribute(string section, string tab)
        {
            Section = section;
            Tab = tab;
        }
    }
}