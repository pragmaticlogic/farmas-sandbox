﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using WebSamples.Models.ViewModels;
using WebSamples.Controllers;

namespace WebSamples
{
    public class NavigationFilter: IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //get the current navigation
            var currentNav = filterContext.ActionDescriptor
                .GetCustomAttributes(typeof(NavigationAttribute), true)
                .Select(n => n as NavigationAttribute)
                .FirstOrDefault();

            // Build the navigation menus.
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                var actions = typeof(SamplesController).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                var navigations = from a in actions
                                  from n in a.GetCustomAttributes(typeof(NavigationAttribute), true)
                                  select new
                                  {
                                      Section = ((NavigationAttribute)n).Section,
                                      Tab = ((NavigationAttribute)n).Tab,
                                      Controller = a.DeclaringType.Name.Replace("Controller", ""),
                                      Action = a.Name
                                  };

                result.ViewBag.Sections = from n in navigations
                                          group n by n.Section into nGroup
                                          select new NavigationInfo
                                          {
                                              Title = nGroup.Key,
                                              Controller = nGroup.First().Controller,
                                              Action = nGroup.First().Action,
                                              IsActive = currentNav != null 
                                                  && currentNav.Section.Equals(nGroup.Key,
                                                                      StringComparison.
                                                                          InvariantCultureIgnoreCase)
                                          };
                if (currentNav == null)
                {
                    result.ViewBag.Tabs = Enumerable.Empty<NavigationInfo>();
                }
                else
                {
                    result.ViewBag.Tabs = from n in navigations
                                          where n.Section.Equals(currentNav.Section, StringComparison.InvariantCultureIgnoreCase)
                                          select new NavigationInfo
                                          {
                                              Title = n.Tab,
                                              Controller = n.Controller,
                                              Action = n.Action,
                                              IsActive =
                                                  currentNav.Tab.Equals(n.Tab,
                                                                      StringComparison.
                                                                          InvariantCultureIgnoreCase)
                                          };
                }
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}