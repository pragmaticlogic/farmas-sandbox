var farmas = (function () {
    var solveRecur = function (index, arr, sol) {
        if (arr[index] === 0) {
            // if all are zero, then s has the solution
            return _.all(arr, function (e) { return e === 0; });
        }

        //push current index to sol
        sol.push(index);
        var moves = arr[index];
        arr[index] = 0;

        //check right
        newIndex = getRightIndex(index, moves, arr.length);
        if (solveRecur(newIndex, arr, sol)) {
            return true;
        }
        //check left
        var newIndex = getLeftIndex(index, moves, arr.length);
        if (solveRecur(newIndex, arr, sol)) {
            return true;
        }

        //this path is wrong;
        arr[index] = moves;
        sol.pop();
        return false;
    }

    var getLeftIndex = function (index, moves, size) {
        for (var count = 0; count < moves; count++) {
            if (--index < 0) {
                index = size - 1;
            }
        }
        return index;
    };

    var getRightIndex = function (index, moves, size) {
        for (var count = 0; count < moves; count++) {
            if (++index >= size) {
                index = 0;
            }
        }
        return index;
    };

    return {
        solve: function (arr) {
            var solution = [];
            for (var index = 0; index < arr.length; index++) {
                if (solveRecur(index, arr, solution)) {
                    return solution;
                }
            }
            return null;
        }
    }
})();
