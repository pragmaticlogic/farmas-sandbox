﻿$(function () {
    // function that formats the terms found in a path.
    var formatPath = function (path, term) {
        var pathToLower = path.toLowerCase();
        var terms = term.toLowerCase().split(' ');
        var blocks = [];

        _.chain(terms)
            .filter(function (term) { return term.length > 0; })
            .each(function (term) {
                addBlocksForTerm(pathToLower, term, blocks);
            });

        blocks = _.sortBy(blocks, 'startIndex');
        // go through all blocks and add formatting
        for (var i = blocks.length - 1; i >= 0; i--) {
            var block = blocks[i];

            path = path.slice(0, block.startIndex) + "<span class='highlight'>" + path.slice(block.startIndex, block.endIndex) + "</span>" + path.slice(block.endIndex);
        }

        return path;
    };

    var addBlocksForTerm = function (path, term, blocks) {
        var startIndex = path.indexOf(term, 0);

        while (startIndex !== -1) {
            var endIndex = startIndex + term.length;

            var block = _.find(blocks, function (b) {
                return (startIndex >= b.startIndex && startIndex <= b.endIndex /*start in middle of block*/)
                        || (endIndex >= b.startIndex && endIndex <= b.endIndex /*end in middle of block*/)
                        || (startIndex < b.startIndex && endIndex > b.endIndex /*block contained inside*/);
            });

            if (block) {
                // this is part of another block, so we must merge
                if (startIndex < block.startIndex) {
                    block.startIndex = startIndex;
                }
                if (endIndex > block.endIndex) {
                    block.endIndex = endIndex;
                }
            }
            else {
                // this is not part of any block, let's add it
                blocks.push({ startIndex: startIndex, endIndex: endIndex });
            }

            startIndex = path.indexOf(term, endIndex + 1);
        }
    };

    // request to build the index
    $.ajax('/Lucene/Index');

    // bound to keypress
    var $text = $('#searchTextbox');
    $text.keyup($.debounce(350, function () {
        var term = $text.val();

        // send request for search
        $.ajax('/Lucene/Search?term=' + term)
            .done(function (data) {
                var $list = $('#fileList').empty();
                _.each(data, function (file) {
                    var $li = $('<li>').append(
                        $('<a>')
                            .html(formatPath(file, term))
                            .attr('href', 'http://aspnetwebstack.codeplex.com/SourceControl/changeset/view/eecfe803d31d#' + file));

                    $li.appendTo($list);
                });
            });
    }));

    $text.focus();
});