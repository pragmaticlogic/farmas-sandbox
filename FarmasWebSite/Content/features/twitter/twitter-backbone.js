﻿$(function () {
    var TwitterModel = Backbone.Model.extend({
        initialize: function () {
            this.max_id = 0;
            this.count = 20;
            this.tweets = [];
        },
        fetch: function () {
            var self = this;
            this.set({ fetching: true });
            if (!self.max_id) {
                self.trigger('loading');
            }
            else {
                self.trigger('appending');
            }
            
            var user = this.get('username');

            if (!user) {
                this.trigger('loaded', []);
                return;
            }

            var dataReceived = false;
            var url = 'https://api.twitter.com/1/statuses/user_timeline.json?screen_name=' + user + '&count=' + this.count + '&callback=?';
            if (self.max_id) {
                url += '&max_id=' + self.max_id;
            }

            var req = $.ajax({
                url: url,
                contentType: 'application/json',
                dataType: 'jsonp'
            });
            req.done(function (data) {
                dataReceived = true;
                self.tweets.push(data);

                if (!self.max_id) {
                    self.trigger('loaded', data);
                }
                else {
                    data.splice(0, 1)
                    self.trigger('appended', data);
                }

                if (data.length > 0) {
                    self.max_id = data[data.length - 1].id;
                }
                self.set({ fetching: false });
            });

            window.setTimeout(function () {
                if (!dataReceived) {
                    self.trigger('error');
                }
            }, 2000);
        }
    });

    var TwitterView = Backbone.View.extend({
        initialize: function () {
            this.template = _.template($('#tweet-template').html());
            this.model.bind('loading', this.loading, this);
            this.model.bind('loaded', this.loaded, this);
            this.model.bind('appending', this.appending, this);
            this.model.bind('appended', this.appended, this);
            this.model.bind('error', this.error, this);
            
            this.itemCount = 0;
        },
        loading: function () {
            $('#tweetSpinner').show();
            this.itemCount = 0;
            this.$el.empty();
            if (this.$el.hasClass('isotope')) {
                this.$el.isotope('destroy');
            }
            return this;
        },
        appending: function () {
            $('#tweetSpinner').show();
            return this;
        },
        error: function () {
            $('#tweetSpinner').hide();
            $('<div>')
                .text('User not found.')
                .addClass('tweet-item tweet-error round')
                .appendTo(this.$el);
            return this;
        },
        addItems: function (data) {
            $('#tweetSpinner').hide();
            var self = this;
            _.each(data, function (t) {
                var $newItem = $(this.template(t))
                
                if((self.itemCount++ % 2) == 0){
                    $newItem.addClass('tweet-content-right-arrow');
                }
                else{
                    $newItem.addClass('tweet-content-left-arrow');                    
                }

                $newItem.appendTo(this.$el);

                $('<div>')
                    .addClass('tweet-item tweet-padding')
                    .appendTo(this.$el);
            }, this);
        },
        appended: function (data) {
            var $last = this.$el.children().last();

            this.addItems(data);

            var $new = $last.nextAll();

            this.$el.isotope('appended', $new);
        },
        loaded: function (data) {
            this.addItems(data)

            this.$el.isotope({
                itemSelector: '.tweet-item'
            });

            return this;
        },
    });

    var model;

    $('#userForm').submit(function (evt) {
        evt.preventDefault();
        model = new TwitterModel({ username: $('#userTextbox').val() })
        var view = new TwitterView({ model: model });
        view.setElement($('#tweetList'));

        model.fetch();
    }).submit();

    $(window).scroll(function () {
        if(model.get('fetching')){
            return;
        }

        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            model.fetch();
        }
    });
});