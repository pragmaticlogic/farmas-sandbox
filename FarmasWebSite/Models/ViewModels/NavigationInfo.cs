﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSamples.Models.ViewModels
{
    public class NavigationInfo
    {
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    
    }
}