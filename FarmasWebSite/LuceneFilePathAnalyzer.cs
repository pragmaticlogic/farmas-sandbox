﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Analysis;

namespace WebSamples
{
    public class LuceneFilePathAnalyzer : StandardAnalyzer
    {
        public LuceneFilePathAnalyzer()
            : base(Lucene.Net.Util.Version.LUCENE_29)
        {
        }   

        public override Lucene.Net.Analysis.TokenStream TokenStream(string fieldName, System.IO.TextReader reader)
        {
            var tokenizer = new LetterTokenizer(reader);
            var stream = new StandardFilter(tokenizer);
            var pascalStream = new LuceneCamelCaseFilter(stream);
            var lowerCaseStream = new LowerCaseFilter(pascalStream);
            return lowerCaseStream;
        }
    }
}