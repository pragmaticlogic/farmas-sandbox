﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lucene.Net.Index;
using Lucene.Net.Store;
using System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;

namespace WebSamples.Controllers
{
    public class LuceneController : Controller
    {
        private const string _luceneFieldName = "path";

        private static IndexReader _reader;
        private static Lucene.Net.Store.Directory _directory = new RAMDirectory();
        private static Analyzer _analyzer = new LuceneFilePathAnalyzer();
        private static bool _indexed = false;
        private static object _syncObject = new object();

        public ActionResult Index()
        {
            if (!_indexed)
            {
                lock (_syncObject)
                {
                    if (!_indexed)
                    {
                        _indexed = true;
                        using (var writer = new IndexWriter(_directory, _analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                        {
                            var path = Server.MapPath("~/Content/features/lucene/aspnetwebstack-files.txt");
                            foreach (var line in System.IO.File.ReadAllLines(path))
                            {
                                var doc = new Document();
                                doc.Add(new Field(_luceneFieldName, line, Field.Store.YES, Field.Index.ANALYZED));
                                writer.AddDocument(doc);
                            }
                        }

                        return Json(new { status = "indexed" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { status = "skipped"} , JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string term)
        {
            if (String.IsNullOrEmpty(term))
            {
                return Json(new string[0], JsonRequestBehavior.AllowGet);
            }

            if (_reader == null)
            {
                _reader = IndexReader.Open(_directory, true);
            }

            var searcher = new IndexSearcher(_reader);

            var parser = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, _luceneFieldName, _analyzer);
            var query = parser.Parse(term.ToLowerInvariant() + "*");
            var collector = TopScoreDocCollector.create(100, true);

            searcher.Search(query, collector);

            var results = collector.TopDocs().ScoreDocs.Select(d => searcher.Doc(d.doc).Get(_luceneFieldName));

            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}
