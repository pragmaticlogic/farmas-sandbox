﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSamples.Controllers
{
    public class SamplesController : Controller
    {
        private const string AreaGames = "Games";
        private const string AreaJQuery = "JQuery/AJAX";

        [Navigation(AreaGames, "Single Player")]        
        public ActionResult Single()
        {
            return View("Canvas/Single-Player");
        }

        [Navigation(AreaGames, "Multi Player")]
        public ActionResult Multi()
        {
            return View("Canvas/Multi-Player");
        }

        [Navigation(AreaJQuery, "Weather")]
        public ViewResult Weather()
        {
            return View("Ajax/Weather");
        }

        [Navigation(AreaJQuery, "Infinite Scrolling")]
        public ActionResult InfiniteScrolling()
        {
            return View("Ajax/Infinite-Scrolling");
        }

        [Navigation(AreaJQuery, "Twitter")]
        public ActionResult Twitter()
        {
            return View("Ajax/Twitter");
        }

        [Navigation(AreaJQuery, "Spiral")]
        public ActionResult Spiral()
        {
            return View("Ajax/Spiral");
        }

        [Navigation(AreaJQuery, "CC.NET Dashboard")]
        public ActionResult CCNetDashboard()
        {
            return View("Ajax/ccnet-dashboard");
        }

        [Navigation("Browser Extensions", "Wunderlist")]
        public ActionResult Wunderlist()
        {
            return View("Extensions/Wunderlist");
        }

        [Navigation("Algorithms", "Final Fantasy XIII-2")]
        public ActionResult FFXIII2()
        {
            return View("Algorithms/FFXIII-2");
        }

        [Navigation("OpenAuth", "OpenId")]
        public ActionResult OpenId()
        {
            return View("OpenAuth/OpenId");
        }

        [Navigation("Lucene", "Lucene")]
        public ActionResult Lucene()
        {
            return View("Lucene/Lucene");
        }
    }
}
