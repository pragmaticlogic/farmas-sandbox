﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNetOpenAuth.OpenId.RelyingParty;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using DotNetOpenAuth.Messaging;

namespace WebSamples.Controllers
{
    public class OpenIdController : Controller
    {
        [HttpGet]
        public ActionResult Logon()
        {
            var openid = new OpenIdRelyingParty();
            IAuthenticationResponse response = openid.GetResponse();

            if (response != null)
            {
                switch (response.Status)
                {
                    case AuthenticationStatus.Authenticated:
                        var claims = response.GetExtension<ClaimsResponse>();
                        TempData.Add("email", claims.Email ?? "<value not returned by provider>");
                        TempData.Add("name", claims.FullName ?? "<value not returned by provider>");
                        break;
                    case AuthenticationStatus.Canceled:
                        TempData.Add("error", "Login was cancelleds at the provider.");
                        break;
                    case AuthenticationStatus.Failed:
                        TempData.Add("error", "Login failed using the provided OpenID identifier.");
                        break;
                }
            }

            return RedirectToAction("OpenId", "Samples");
        }

        [HttpPost]
        public ActionResult Logon(string openid_identifier)
        {
            if (!Identifier.IsValid(openid_identifier))
            {
                TempData.Add("error", "The specified login identifier is invalid");
                return RedirectToAction("OpenId", "Samples");
            }
            else
            {
                var openid = new OpenIdRelyingParty();
                IAuthenticationRequest request = openid.CreateRequest(
                    Identifier.Parse(openid_identifier));

                request.AddExtension(new ClaimsRequest
                {
                    BirthDate = DemandLevel.NoRequest,
                    Email = DemandLevel.Require,
                    FullName = DemandLevel.Require
                });

                return request.RedirectingResponse.AsActionResult();
            }
        }

    }
}
